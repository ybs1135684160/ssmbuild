package com.shirt.service;

import com.shirt.pojo.Books;
import org.springframework.stereotype.Service;

import java.util.List;

public interface BookService {
    //    增加一本书
    int addBook(Books books);

    //    删除一本书
    int deleteBookById(Integer id);

    //    更新一本书
    int updateBook(Books books);

    //    查询一本书
    Books quertBookById(Integer id);

    //    查询全部书
    List<Books> queryAllBook();

    //名称查询书籍
    List<Books> quertBookByName(String bookName);
}
