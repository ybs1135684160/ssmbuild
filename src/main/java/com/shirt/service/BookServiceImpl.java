package com.shirt.service;

import com.shirt.dao.BookMapper;
import com.shirt.pojo.Books;

import java.util.List;

/**
 * Service业务层
 */
public class BookServiceImpl implements BookService {
    //   调用dao层
    private BookMapper bookMapper;

    public void setBookMapper(BookMapper bookMapper) {
        this.bookMapper = bookMapper;
    }

    public int addBook(Books books) {
        return bookMapper.addBook(books);
    }

    public int deleteBookById(Integer id) {
        return bookMapper.deleteBookById(id);
    }

    public int updateBook(Books books) {
        return bookMapper.updateBook(books);
    }

    public Books quertBookById(Integer id) {
        return bookMapper.quertBookById(id);
    }

    public List<Books> queryAllBook() {
        return bookMapper.queryAllBook();
    }

    public List<Books> quertBookByName(String bookName) {
        return bookMapper.quertBookByName(bookName);
    }
}
