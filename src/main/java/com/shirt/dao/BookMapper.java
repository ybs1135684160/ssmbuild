package com.shirt.dao;

import com.shirt.pojo.Books;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BookMapper {
    //    增加一本书
    int addBook(Books books);

    //    删除一本书
    int deleteBookById(@Param("bookID") Integer id);

    //    更新一本书
    int updateBook(Books books);

    //    ID查询一本书
    Books quertBookById(@Param("bookID") Integer id);

    //    查询全部书
    List<Books> queryAllBook();

    //名称查询书籍
    List<Books> quertBookByName(@Param("bookName") String bookName);

}
