package com.shirt.controller;

import com.shirt.pojo.Books;
import com.shirt.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.awt.print.Book;
import java.util.List;

@Controller
@RequestMapping("/book")
public class BookController {

//    @Autowired
    @Qualifier("bookServiceImpl")
    private BookService bookService;

    /**
     * 查询全部
     *
     * @param model
     * @return
     */
    @RequestMapping("/allBook")
    public String allBook(Model model) {
        List<Books> list = bookService.queryAllBook();
        model.addAttribute("queryAllBooklist", list);
        return "allBook";
    }

    /**
     * 跳转添加页面
     *
     * @return
     */
    @RequestMapping("/toAddBook")
    public String toAddPaper() {
        return "toAddBook";
    }

    /**
     * 跳转主页
     *
     * @return
     */
    @RequestMapping("/toIndex")
    public String toIndex() {
        return "redirect:/index.jsp";
    }

    /**
     * 添加
     *
     * @param books
     * @return
     */
    @RequestMapping("/addBook")
    public String addBook(Books books) {
        System.out.println("addbook=" + books);
        bookService.addBook(books);
//        重定向到@RequestMapping("/allBook")，调用了一次查询全部
        return "redirect:/book/allBook";
    }


    /**
     * 跳转修改页面
     *
     * @return
     */
    @RequestMapping("/toUpdateBook/{id}")
    public String todUpdateBook(@PathVariable("id") int id, Model model) {
        Books books = bookService.quertBookById(id);
        model.addAttribute("QBooks", books);
        return "updateBook";
    }

    /**
     * 修改
     */
    @RequestMapping("/updateBook")
    public String updateBook(Books books) {
        System.out.println("updateBook=" + books);
        int i = bookService.updateBook(books);
        System.out.println("是否成功=" + i);
        return "redirect:/book/allBook";
    }

    /**
     * 跳转按名称查询
     */
    @RequestMapping("/toSeleceBName")
    public String seleteBookName(Model model) {
        List<Books> books = bookService.queryAllBook();
        model.addAttribute("queryBooksByName", books);
        return "selectBooksByName";
    }

    /**
     * 按名称查询
     *
     * @param model
     * @return
     */
    @RequestMapping("/selectBookName")
    public String toSelectBookName(String bookName, Model model) {
        System.out.println("bookName=" + bookName);
        List<Books> books = bookService.quertBookByName(bookName);
        model.addAttribute("queryBooksByName", books);
        return "selectBooksByName";
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @RequestMapping("/del/{id}")
    public String delete(@PathVariable("id") int id) {
        System.out.println("id=" + id);
        int i = bookService.deleteBookById(id);
        System.out.println("是否成功=" + i);
        return "redirect:/book/allBook";
    }
}