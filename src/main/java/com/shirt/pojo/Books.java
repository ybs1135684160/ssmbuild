package com.shirt.pojo;

import lombok.Data;

@Data
public class Books {
    private Integer bookID;
    private String bookName;
    private Integer bookCounts;
    private String detail;
}
