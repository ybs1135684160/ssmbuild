<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>添加书籍</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <%--    引入公共样式--%>
    <%@include file="CommonStyle.jsp" %>
</head>
<body>
<div class="container">
    <div class="row clearfix">
        <div class="col-md-12 column">
            <div class="page-header">
                <h1>
                    <small>添加书籍</small>
                </h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="column" style="float: right;margin-right: 15px;">
            <a class="btn btn-primary" href="${pageContext.request.contextPath}/book/allBook" value="返回">返回</a>
        </div>
    </div>
    <form action="${pageContext.request.contextPath}/book/addBook" method="get">
        <div class="form-group">
            <label for="bkname">书籍名称：</label>
            <input type="text" name="bookName" class="form-control" id="bkname" required>
        </div>
        <div class="form-group">
            <label for="bkcounts">书籍数量：</label>
            <input type="text" name="bookCounts" class="form-control" id="bkcounts" required>
        </div>
        <div class="form-group">
            <label for="bkdetail">书籍描述：</label>
            <input type="text" name="detail" class="form-control" id="bkdetail" required>
        </div>
        <div class="form-group">
            <input type="reset" class="btn btn-primary" style="background-color: #ff3e49"/>
            <input type="submit" class="btn btn-primary" value="添加"/>
        </div>
    </form>
</div>
</body>
</html>
