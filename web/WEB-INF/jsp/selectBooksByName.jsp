<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: SHIRT
  Date: 2020/7/9
  Time: 9:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>名称查询</title>
    <%@ include file="CommonStyle.jsp" %>
</head>
<body>
<div class="container">
    <div class="row clearfix">
        <div class="col-md-12 column">
            <div class="page-header">
                <h1>
                    <small>书籍列表 —— 名称查询</small>
                </h1>
            </div>
            <div>
                <form action="${pageContext.request.contextPath}/book/selectBookName" method="get">
                    <div class="row">
                        <div class="col-md-2 column" style="padding: 4px;margin-left: 13px;">
                            <input id="bookName" type="text" name="bookName" style="display:inline"/>
                        </div>
                        <div class="column col-md-2">
                            <input class="btn btn-primary" type="submit" value="查询"/>
                        </div>
                        <div class="column" style="float: right;margin-right: 15px;">
                            <a class="btn btn-primary" href="${pageContext.request.contextPath}/book/allBook" value="返回">返回</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-md-12 column">
            <table class="table table-hover table-striped">
                <thead>
                <tr>
                    <th>书籍编号</th>
                    <th>书籍名字</th>
                    <th>书籍数量</th>
                    <th>书籍详情</th>
                    <th>操作</th>
                </tr>
                </thead>

                <tbody>
                <c:forEach var="book" items="${queryBooksByName}">
                    <tr>
                        <td>${book.bookID}</td>
                        <td>${book.bookName}</td>
                        <td>${book.bookCounts}</td>
                        <td>${book.detail}</td>
                        <td>
                            <a href="${pageContext.request.contextPath}/book/toUpdateBook/${book.bookID}">更改</a>
                            |
                            <a href="${pageContext.request.contextPath}/book/del/${book.bookID}">删除</a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
